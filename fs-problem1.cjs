/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/



//====================================================================================


const fs = require('fs');

const path = require('path');


function problem1(absolutePathOfRandomDirectory, randomNumberOfFiles) {

    const files = [];
    
    fs.mkdir(absolutePathOfRandomDirectory, function (err) {
        if (err) {
            console.error(err);
        } else {
            console.log('folder created successfully.');
            createFiles();
        };
    });



    function createFiles() {
        
        for (let index = 1; index <= randomNumberOfFiles; index++) {
            // console.log(files);
            let filePath = path.resolve(absolutePathOfRandomDirectory, `file${index}.json`);
            fs.writeFile(filePath, "{}", function (err) {
                if (err) {
                    console.error(err);
                } else {
                    console.log("Success");
                    files.push(`file${index}.json`);
                    if (files.length === randomNumberOfFiles) {
                        // console.log(files);
                        deleteFile();
                    }
                }
            });
        }
    };




    function deleteFile() {
        for (let index = 0; index < files.length; index++) {
            const filePath = path.resolve(absolutePathOfRandomDirectory, files[index]);
            fs.unlink(filePath, function (err) {
                if (err) {
                    console.error(err);
                } else {
                    console.log("Deleted");
                }
            })
        };
    };
};

// problem1("./random", 10);


module.exports = problem1;

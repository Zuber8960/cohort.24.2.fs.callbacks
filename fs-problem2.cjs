
/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt.
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt.
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt.
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.

*/



//=============================================================================================


const fs = require('fs');
const path = require('path');

const filePath = path.resolve("lipsum.txt");
const fileNames = path.resolve("filenames.txt");
const file1 = path.resolve("file1.txt");
const file2 = path.resolve("file2.txt");
const file3 = path.resolve("file3.txt");


function problem2() {

    function convertToUpperCaseAndWriteToNewFile(filePath) {
        fs.readFile(filePath, "utf-8", function (err, data) {
            if (err) {
                console.log(err);
            } else {
                data = data.toUpperCase();
                sendDataInFile(file1, data);
            }
        })
    }

    function sendDataInFile(file, content) {
        fs.writeFile(file, content, function (err) {
            if (err) {
                console.log(err);
            } else {

                file = file.split("/");
                const name = file[file.length - 1];
                console.log(`${name} is created successfully !` )
                if (name === "file1.txt") {
                    writtingNamesAndNextOperation(fileNames, name);
                } else {
                    appendNamesAndOtherOperations(fileNames, name);
                };
            }
        })
    }

    function writtingNamesAndNextOperation(fileNames, content) {
        const fileName = content;
        content += "\n";
        fs.writeFile(fileNames, content, function (err) {
            if (err) {
                console.log(err);
            } else {
                console.log(`${fileName} has written in file filenames.txt`);
                convertToLowerCaseAndWriteToNewFile(file1);
            }
        })
    };


    function appendNamesAndOtherOperations(fileNames, content) {
        const fileName = content;
        if (content !== "file3.txt") {
            content += "\n";
        }
        fs.appendFile(fileNames, content, function (err) {
            if (err) {
                console.log(err);
            } else {
                console.log(`${fileName} has appended in file filenames.txt`);
                if (content.includes("file2")) {
                    readFileAndSorting(file2);
                } else if (content === "file3.txt") {
                    deleteFiles();
                }
            }
        })
    }


    convertToUpperCaseAndWriteToNewFile(filePath);




    function convertToLowerCaseAndWriteToNewFile(newFile) {
        fs.readFile(newFile, "utf-8", function (err, data) {
            if (err) {
                console.log(err);
            } else {
                // console.log(data);
                data = data.toLowerCase().split(".").join("\n");
                sendDataInFile(file2, data);
            }
        })
    }



    function readFileAndSorting(file) {
        fs.readFile(file, "utf-8", function (err, data) {
            if (err) {
                console.log(err);
            } else {
                data = data.split("\n")
                    .sort((first, second) => {
                        return first - second;
                    })
                    .join("\n");
                // console.log(data);
                sendDataInFile(file3, data);
            }
        })
    }



    function deleteFiles() {
        fs.readFile(fileNames, "utf-8", function (err, data) {
            if (err) {
                console.log(err);
            } else {
                data = data.split("\n");

                // console.log(data);
                data.map(file => {
                    let deleteFilePath = path.resolve(file);
                    fs.unlink(deleteFilePath, function (err) {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log(`${file} deleted successfully !`);
                        };
                    })
                })

            }
        })
    }
}

// problem2();


module.exports = problem2;


//=============================================================================================
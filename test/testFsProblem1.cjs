const problem1 = require("../fs-problem1.cjs");
const path = require("path");

const absolutePathOfRandomDirectory = path.resolve("random");
const  randomNumberOfFiles = 10;

problem1(absolutePathOfRandomDirectory, randomNumberOfFiles);